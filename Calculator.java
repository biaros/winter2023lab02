public class Calculator
{
	public static double addNums(double num1, double num2)
	{
		double sum = num1 + num2;
		return(sum);
	}
	
	public static double subtractNums(double num1, double num2)
	{
		double difference = num1 - num2;
		return(difference);
	}
	
	public double multiplyNums(double num1, double num2)
	{
		double product = num1 * num2;
		return(product);
	}
	
	public double divideNums(double num1, double num2)
	{
		double quotient = num1/num2;
		return(quotient);
	}
}