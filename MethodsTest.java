public class MethodsTest
{
	public static void main(String[] args)
	{
		/*int x = 5;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		methodOneInputNoReturn(x+10);
		System.out.println(x);
		methodTwoInputNoReturn(5, 5.5);
		int num = methodNoInputReturnInt();
		System.out.println(num);
		double root = sumSquareRoot(9, 5);
		System.out.println(root);
		String s1 = "java";
		String s2 = "programming";
		System.out.println(s1.length());
		System.out.println(s2.length()); */
		
		System.out.println(SecondClass.addOne(50));
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
	}
	
	public static void methodNoInputNoReturn()
	{
		System.out.println("i'm a method that takes no input and returns nothing");
		int x = 20;
		System.out.println(x);
	}
	public static void methodOneInputNoReturn(int num)
	{
		System.out.println("inside the method one input no return");
		num = num - 5;
		System.out.println(num);
	}
	
	public static void methodTwoInputNoReturn(int numInt, double numDouble)
	{
		System.out.println("inside method two input no return");
		System.out.println(numInt);
		System.out.println(numDouble);
	}
	public static int methodNoInputReturnInt()
	{
		System.out.println("inside method no input return int");
		return(5);
	}
	
	public static double sumSquareRoot(int num1, int num2)
	{
		double root = num1+num2;
		root = Math.sqrt(root);
		return root;
	}
}